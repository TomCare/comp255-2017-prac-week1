
### Who/What is this repository for? ###

* COMP255 students enrolled in 2017 S2 at Macquarie University
* to test automatic submission of coding exercises 

### How do I get set up? ###

* Fork this repository under your Bitbucket account
* Modify the file (HelloWorld.java) as you wish
* Commit your version of HelloWorld.java to your master branch
* Submit your assignment (from master) using [BBmarking at Macquarie](https://bbmarking.science.mq.edu.au/bbmarking/)