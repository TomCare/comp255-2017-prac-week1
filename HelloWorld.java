
/**
 *    This is a simple program to print hello world.
 *    You have to modify it and print your mq account instead
 *    i.e. the output should be Hello <your OneID>
 */
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Here we are");

        // Prints "Hello, World" in the terminal window.
        System.out.println("Hello Brave New World");
        System.out.println("And bye");
    }
}
